package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"os"
)

type CabDetails struct{
	PickUpDate []string `json:"pickUpDate"`
}

type Cab struct {
	Medallion string `json:"medallion"`
	TotalTrips int  `json:"totalTrips"`
	Details  CabDetails `json:"details"`
}

func DB() (*sql.DB,error){
	db, err := sql.Open(os.Getenv("DB_DRIVER"), os.Getenv("DB_DATASOURCE"))
	return db,err
}
