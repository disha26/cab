package models

import (
	"database/sql"
	"encoding/json"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"gitlab.com/disha26/cab/api/utils"
	"log"
)

func TripDetails(cabIds []string, nocache string) ([]Cab, error){
	db,err := DB()
	defer db.Close()
	if err != nil {
		return nil, errors.Wrap(err, "Error connecting to mysql")
	}
	redisClient := Redis()
	trips := []Cab{}
	for _, medallion := range cabIds {
		redisKey, err  := redisClient.Get(medallion).Result()
		cab := Cab{}
		if nocache == "1" || err == redis.Nil {
			log.Printf("info: %s\n","Fetching values from database")
			cab, err = GetCabDetailsFromDatabase(db,medallion,redisClient)
			if err != nil {
				return nil, err
			}

		} else {
			log.Printf("info: %s\n","Fetching values from cache")
			cab, err = GetCabDetailsFromRedis(redisKey)
			if err != nil {
				return nil, err
			}
		}
		trips = append(trips, cab)
	}
	return trips, nil
}

func GetCabDetailsFromRedis(redisKey string) (Cab,error){
	in := []byte(redisKey)
	data := Cab{
		Details: CabDetails{},
	}
	if err := json.Unmarshal(in, &data); err != nil {
		return data,err
	}
	return data,nil
}

func GetCabDetailsFromDatabase(db *sql.DB, medallion string, redisClient *redis.Client ) (Cab, error) {
	cab := Cab{}
	sqlStatement := "SELECT DATE(pickup_datetime) FROM cab_trip_data WHERE medallion = '" + medallion + "'"
	results, sqlErr := db.Query(sqlStatement)
	defer results.Close()
	if sqlErr != nil {
		return cab, errors.Wrap(sqlErr, "Error while running the sql command to fetch trip details")
	}

	var cabDetails CabDetails
	count := 0
	for results.Next() {
		count++
		var result string
		err := results.Scan(&result)
		if err != nil {
			return cab, err
		}
		cabDetails.PickUpDate = utils.AppendIfMissing(cabDetails.PickUpDate, result)

	}
	cab = Cab{Medallion: medallion, TotalTrips: count, Details: cabDetails}
	out, m_err := json.Marshal(cab)
	if m_err != nil {
		return cab, m_err
	}
	//No need to clear the cache as it will override the key
	log.Printf("info: %s\n","Writing to cache")
	err := redisClient.Set(medallion, string(out), 0).Err()

	//ToDo: do something as return utils.ThrowError(cab,err)

	if err != nil {
		return cab,err
	}
	return cab, nil

}
