package models

import (
	"github.com/go-redis/redis"
	"log"
	"os"
	"strconv"
)

func ClearCache(context string) error{
	client := Redis()
	log.Printf("info: %s\n","Clearing cache")
	iter := client.Scan(0, context, 0).Iterator()
	for iter.Next() {
		err := client.Del(iter.Val()).Err()
		if err != nil { return err }
	}
	if err := iter.Err(); err != nil {
		return err
	}
	return nil

}

func Redis() *redis.Client {
	db, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		db = 0
	}
	redisClient := redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_ADDR"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB: db,
	})
	return redisClient
}

