package models

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	Status string `json:"status"`
	Code string `json:"code"`
	Message string `json:"message"`
	Cabs []Cab `json:"cabs"`
}

func PrintResponse(responseWriter http.ResponseWriter, i interface{} ){
	data, err := json.Marshal(i)
	if err != nil {}
	responseWriter.Header().Add("Content-Type", "application/json; charset=utf-8")
	responseWriter.Write(data)
}

//Keeping it simple with same code and status, but in future based on different codes different messages can be shown to the user

func PrintCabSuccessResponse(responseWriter http.ResponseWriter, cab []Cab ){
	responseWriter.WriteHeader(http.StatusOK)
	response := Response{Status: "success", Code: "200", Cabs: cab}
	PrintResponse(responseWriter, response)
}

func PrintCabFailureResponse(responseWriter http.ResponseWriter){
	responseWriter.WriteHeader(http.StatusBadRequest)
	response := Response{Status: "fail", Code: "400"}
	PrintResponse(responseWriter, response)
}

func PrintFailureResponse(responseWriter http.ResponseWriter, msg string){
	responseWriter.WriteHeader(http.StatusBadRequest)
	response := Response{Status: "fail", Code: "400", Message: msg}
	PrintResponse(responseWriter, response)
}

func PrintSuccessResponse(responseWriter http.ResponseWriter, msg string){
	responseWriter.WriteHeader(http.StatusOK)
	response := Response{Status: "success", Code: "200", Message: msg}
	PrintResponse(responseWriter, response)
}

