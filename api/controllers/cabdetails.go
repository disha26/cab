package controllers

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/disha26/cab/api/models"
	"log"
	"net/http"
)


func GetCabDetails( responseWriter http.ResponseWriter, request  *http.Request) {
	cabIds, ok := request.URL.Query()["id"]
	if !ok || len(cabIds[0]) < 1 {
		models.PrintFailureResponse(responseWriter, "Cab id is missing from the url")
		return
	}
	isCacheSet,ok := request.URL.Query()["nocache"]
	nocache := "0"
	if ok && len(isCacheSet[0]) == 1 {
		nocache = isCacheSet[0]
	}

	cabs, err := models.TripDetails(cabIds, nocache)
	if err != nil {
		log.Printf("error: %s\n",err)
		models.PrintCabFailureResponse(responseWriter)
	}else{
		models.PrintCabSuccessResponse(responseWriter,cabs)
	}

}





