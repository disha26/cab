package controllers
import (
	"gitlab.com/disha26/cab/api/models"
	"log"
	"net/http"
)

func DeleteCache(responseWriter http.ResponseWriter, req *http.Request) {
	err := models.ClearCache("*")
	if err == nil {
		models.PrintSuccessResponse(responseWriter, "Cache is cleared")
	}else{
		log.Printf("error: %s\n",err)
		models.PrintFailureResponse(responseWriter, "Server error")
	}
}
