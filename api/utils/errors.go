package utils

func ThrowError(x interface{}, err error) (interface{},error){
	if(err != nil){
		return x,err
	}
	return x,nil
}
