package utils

import (
	"log"
	"os"
)

func Logger(){
	loggerSetUp()
}

func loggerSetUp() {
	out, err := os.OpenFile("cab.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return
	}
	log.SetOutput(out)
}
