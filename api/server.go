package api

import (
	"gitlab.com/disha26/cab/api/controllers"
	"net/http"
	"os"
)

func Run() {
	http.HandleFunc("/", controllers.Initialize)
	http.HandleFunc("/getcabdetails", controllers.GetCabDetails)
	http.HandleFunc("/clearcache", controllers.DeleteCache)
	http.ListenAndServe(port(),nil)
}

func port() string {
	port := os.Getenv("SERVER_PORT")
	if len(port) == 0{
		port = "8080"
	}
	return ":" + port
}

