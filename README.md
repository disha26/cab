# Cab API

This project would expose some api's which can be used to fetch cab details.

#Set up
1. Set redis, mysql server and go server details in env file
2. Import provided sql file in mysql server
3. Use go build main.go

#API List
1. /clearcache - This will clear the complete redis cache

2. /getcabdetails?id=<medallion1>&id=<medallion2>&.... - This will fetch details with respect to cab id.
It will first check in redis, if cached, will return else will query the database and cache the values.
3. /getcabdetails?nocache=1&id=<medallion>&id=.. - This will fetch the value from database and override
the cache for the provided cab id.

#ToDos

1. cli tool
2. Redis and sql pooling
3. Custom error handler, logger, router
4. Script/Dockerizing to avoid setup
5. Tests
6. Comments improvement




