package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/disha26/cab/api"
	"gitlab.com/disha26/cab/api/utils"
)

func main() {
	godotenv.Load(".env")
	utils.Logger()
	api.Run()
}
